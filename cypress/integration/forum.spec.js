describe('Reach Forum page', function () {
  it('page existed', function () {
    expect(true).to.equal(true)
  })
})
describe('The forum Page', function() {
  it('successfully loads', function() {
    cy.visit('http://localhost:8080/#/forum')
  })
})

describe('Forum page test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/forum')
  })
  it('should diplay forum page', function () {
    cy.get('div').should('have.class', 'hero')
    cy.get('h3').should('have.class', 'vue-title')
  })
})
