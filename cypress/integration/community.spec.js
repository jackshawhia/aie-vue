describe('Reach Community page', function () {
  it('page existed', function () {
    expect(true).to.equal(true)
  })
})
describe('The Community Page', function () {
  it('successfully loads', function () {
    cy.visit('http://localhost:8080/#/community')
  })
})
describe('Community page test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/community')
  })
  it('should display community page', function () {
    cy.get('div').should('have.class', 'hero')
    cy.get('a').should('have.class', 'fa fa-eye fa-2x')
    cy.get('a').should('have.class', 'fa fa-trash-o fa-2x')
    cy.get('a').should('have.class', 'fa fa-edit fa-2x')
    cy.get('h3').should('have.class', 'vue-title')
    cy.get('h3').should('have.class', 'medium')
  })
})
