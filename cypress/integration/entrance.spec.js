describe('Reach Entrance page', function () {
  it('page existed', function () {
    expect(true).to.equal(true)
  })
})
describe('The Entrance Page', function () {
  it('successfully loads', function () {
    cy.visit('http://localhost:8080/#')
  })
})
describe('entrance test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#')
  })
  it('should display entrance page', function () {
    cy.get('div').should('have.class', 'left')
    cy.get('div').should('have.class', 'line')
    cy.get('div').should('have.class', 'line-in')
    cy.get('div').should('have.class', 'right')
    cy.get('div').should('have.class', 'left-content')
    cy.get('div').should('have.class', 'right-content')
    cy.get('span').should('have.class', 'blackblock')
    cy.get('span').should('have.class', 'redblock')
  })
})
describe('entrance functionality', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#')
  })
  it('direct to required page', function () {
    cy.get('div').should('have.class', 'right-content')
    cy.get('span').should('have.class', 'redblock')
  })
})
describe('navigation bar', () => {
  it('Shows required links', () => {
    cy.get('.navbar').contains('Home')
    cy.get('.navbar-nav:nth-child(1)').within(() => {
      cy.get('.nav-item:first').should('contain', 'Home')
      cy.get('.nav-item:nth-child(2)').should('contain', 'Community')
      cy.get('.nav-item:nth-child(3)').should('contain', 'Forum')
      cy.get('.nav-item:nth-child(4)').should('contain', 'Map')
    })
    cy.get('.navbar-nav:nth-child(2)').within(() => {
      cy.get('.nav-item:first').should('contain', 'Publish')
      cy.get('.nav-item:nth-child(2)').should('contain', 'About')
      cy.get('.nav-item:nth-child(3)').should('contain', 'Support')
    })
  })
  it('Redirects to home when clicked', () => {
    cy.get('.navbar').contains('Home').click()
    cy.url().should('include', '/entrance')
  })
  it('Redirects to forum when clicked', () => {
    cy.get('.navbar').contains('Forum').click()
    cy.url().should('include', '/forum')
  })
  it('Redirects to map when clicked', () => {
    cy.get('.navbar').contains('Map').click()
    cy.url().should('include', '/map')
  })
  it('Redirects to publish page when clicked', () => {
    cy.get('.navbar').contains('Publish').click()
    cy.url().should('include', '/publish')
  })
  it('Redirects to about page when clicked', () => {
    cy.get('.navbar').contains('About').click()
    cy.url().should('include', '/about')
  })
  it('Redirects to support when clicked', () => {
    cy.get('.navbar').contains('Support').click()
    cy.url().should('include', '/support')
  })
})
