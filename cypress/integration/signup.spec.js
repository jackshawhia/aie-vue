// let db = mongoose.connection;
describe('Reach Sign Up page', function () {
  it('page existed', function () {
    expect(true).to.equal(true)
  })
})
describe('The Sign Up Page', function() {
  it('successfully loads', function() {
    cy.visit('http://localhost:8080/#/SignUp')
  })
})
describe('Sign up test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/SignUp')
  })
  it('should diplay sign up interface', function () {
    cy.get('div').should('have.class', 'hero')
    cy.get('div').should('have.class', 'hero')
    cy.get('div').should('have.class', 'foot-lnk')
  })
  it('allows a customer to sign up', () => {
    // Fill out web form cy.get(':nth-child(1) > .form__input')
    cy.get('.col-md-6').find(':nth-child(1) > .form__input').type(2100999)
    cy.get('[data-test=name]').type('angle')
    cy.get('[data-test=email]').type('772012459@qq.com')
    cy.get('div[data-test="password"] > [data-test=password]').type(123456)
    cy.get('.error').should('not.exist')
    cy.get('.btn').click()
    // cy.get('.col-md-6').find(':nth-child(6)').find('.btn').click()
    cy.contains('SignUp Successfully!').should('exist')
  })
})

