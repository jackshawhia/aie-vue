describe('Reach Publish page', function () {
  it('page existed', function () {
    expect(true).to.equal(true)
  })
})

describe('The Publish Page', function () {
  it('successfully loads', function () {
    cy.visit('http://localhost:8080/#/publish')
  })
})
describe('Publish page test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/publish')
  })
  it('should display publish interface', function () {
    cy.get('div').should('have.class', 'hero')
    cy.get('h3').should('have.class', 'vue-title')
    cy.get('div').should('have.class', 'container mt-3 mt-sm-5')
    cy.get('div').should('have.class', 'row justify-content-center')
    cy.get('div').should('have.class', 'col-md-6')
    cy.get('div').should('have.class', 'form-group')
    cy.get('label').should('have.class', 'form-label')
    cy.get('select').should('have.class', 'form-control')
    cy.get('label').should('have.class', 'form__label')
    cy.get('input').should('have.class', 'form__input')
    cy.get('div').should('have.class', 'error')
    cy.get('button').should('have.class', 'btn btn-primary btn1')
    cy.get('a').should('have.class', 'btn btn-primary btn1')
    cy.get('div').should('have.class', 'foot-lnk')
    // cy.get('p').should('have.class', 'typo_p')
  })
})
