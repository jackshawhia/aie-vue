describe('Reach About Us page', function () {
  it('page existed', function () {
    expect(true).to.equal(true)
  })
})

describe('The About Page', function () {
  it('successfully loads', function () {
    cy.visit('http://localhost:8080/#/about')
  })
})
describe('About page test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/about')
  })
  it('should diplay about page', function () {
    cy.get('div').should('have.class', 'hero')
    cy.get('h3').should('have.class', 'vue-title')
  })
})
