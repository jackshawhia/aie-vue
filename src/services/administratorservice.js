import Api from '@/services/api'
export default {
  fetchMembers () {
    return Api().get('/member')
  },
  Login (admin) {
    return Api().post('/admin/login', admin,
      { header: {'Content-type': 'application/json'} })
  },
  Logout () {
    return Api().post('/member/logout')
    // { header: {'Content-type': 'application/json'} })
  },
  fetchAMember (email) {
    return Api().get(`/member/${email}`)
  },
  changePass (member, one, token) {
    // console.log('REQUESTING ' + user.email + ' ' +
    //   JSON.stringify(user, null, 5))
    return Api().put(`/member/changePassword/${member}`, one,
      { headers: {'Content-type': 'application/json', 'token': token} })
  }
}
