import Api from '@/services/api'
export default {
  fetchArtwork () {
    return Api().get('/artwork')
  },
  postArtwork (artwork) {
    return Api().post('/artwork', artwork,
      { headers: {'Content-type': 'application/json'} })
  },
  viewArtwork (id) {
    // eslint-disable-next-line no-template-curly-in-string
    return Api().put(`/artwork/${id}/view_times`)
  },
  // eslint-disable-next-line camelcase
  removeArtwork (art_name) {
    // eslint-disable-next-line camelcase
    return Api().delete(`/artwork/${art_name}`)
  },
  fetchOne (id) {
    return Api().get(`/donations/${id}`)
  },
  putArtwork (id, artwork) {
    console.log('REQUESTING ' + artwork._id + ' ' +
      JSON.stringify(artwork, null, 5))
    return Api().put(`/artwork/${id}`, artwork,
      { headers: {'Content-type': 'application/json'} })
  }
}
