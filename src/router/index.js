import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Community from '@/components/Community'
import Publish from '@/components/Publish'
import About from '@/components/About'
import ContactUs from '@/components/ContactUs'
import Forum from '@/components/Forum'
import Support from '@/components/Support'
import SignUp from '@/components/SignUp'
import Login from '@/components/Login'
import Logout from '@/components/Logout'
import Entrance from '@/components/Entrance'
import Map from '@/components/Map'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/map',
      name: 'Map',
      component: Map
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout
    },
    {
      path: '/community',
      name: 'Community',
      component: Community
    },
    {
      path: '/entrance',
      name: 'Entrance',
      component: Entrance
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/contact',
      name: 'ContactUs',
      component: ContactUs
    },
    {
      path: '/publish',
      name: 'Publish',
      component: Publish
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/forum',
      name: 'Forum',
      component: Forum
    },
    {
      path: '/support',
      name: 'support',
      component: Support
    }
  ]
})
