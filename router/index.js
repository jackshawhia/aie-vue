import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Community from '@/components/Community'
import Publish from '@/components/Publish'
import About from '@/components/About'
import ContactUs from '@/components/ContactUs'
import Forum from '@/components/Forum'
import Support from '@/components/Support'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/community',
            name: 'Community',
            component: Community
        },
        {
            path: '/donate',
            name: 'Donate',
            component: Donate
        },
        {
            path: '/about',
            name: 'AboutUs',
            component: AboutUs
        },
        {
            path: '/contact',
            name: 'ContactUs',
            component: ContactUs
        },
        {
            path: '/publish',
            name: 'Publish',
            component: Publish
        },
        {
            path: '/about',
            name: 'About',
            component: About
        },
        {
            path: '/forum',
            name: 'Forum',
            component: Forum
        },
        {
            path: '/support',
            name: 'support',
            component: Support
        }
    ]
})
