# Assignment 2 - Agile Software Practice.

Name: Zhenyu Shao
Student id: 20086425

## Client UI.




>>Allows the user to sign up
>>
![][Sign Up]

>>Allow the user to login
>>
![][Login][https://gitlab.com/jackshawhia/aie-vue/blob/master/src/screenshot/Login.png]

>>Allow the user to view, remove, edit artwork(1st part)
>>
![][Community][https://gitlab.com/jackshawhia/aie-vue/blob/master/src/screenshot/community.png]

>>Allow the user to view, remove, edit artwork(2nd part)
>>
![][Community2][https://gitlab.com/jackshawhia/aie-vue/blob/master/src/screenshot/community2.png]

>>All the user to view entrance, direct to other pages
>>
![][Entrance][https://gitlab.com/jackshawhia/aie-vue/blob/master/src/screenshot/entrance.png]

>>Publish artworks
>>
![][Publish Artworks][https://gitlab.com/jackshawhia/aie-vue/blob/master/src/screenshot/publish.png]

>>View Map
>>
![][Map][https://gitlab.com/jackshawhia/aie-vue/blob/master/src/screenshot/map.png]


## E2E/Cypress testing.

![][Cypress]


## GitLab CI.

![][surge]
![][surge-entrance]


[Map]:./src/screenshot/map.png
[Publish Artworks]:./src/screenshot/publish.png
[Entrance]:./src/screenshot/entrance.png
[Login]:./src/screenshot/Login.png
[Community]:./src/screenshot/community.png
[Community2]:./src/screenshot/community2.png
[Sign Up]:./src/screenshot/signup.png
[cypress]:./src/screenshot/cypress.png
[surge]:./src/screenshot/surge.png
[surge-entrance]:./src/screenshot/surge-entrance.png
